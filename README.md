# Prueba Técnica
Solución de la prueba técnica.
## Rutas de la página

### Personas
| Función | URL | 
| :---: | :---: | 
| Lista de personas | `/` |  
| Registrar persona | `/crear` |  
| Modificar persona | `/modificar/:id` |  

## Ejecución
Para la ejecución de este proyecto requiere tener previamente instalado de `Nodejs` y `npm` para la instalación de dependencias.

Para ejecutar localmente el proyecto, ejecute el siguiente comando:
```javascript
// Instalación de dependencias
$ npm install 

// Ejecución de proyecto 
$ npm start  
```

Para crear el build:
```javascript 
$ npm run build
```
Esto te creara un `build` para subirlo y ejecutarlo en un servidor.

## Imágenes de las vistas

### Lista
![Lista de personas](imgs/list.png)

### Crear
![Crear persona](imgs/create.png)

### Respuesta de crear
![Crear persona respuesta](imgs/create-response.png) 

### Modificar persona
![Modificar persona](imgs/edit.png)

### Eliminar persona
![Eliminar persona](imgs/delete.png)

### Respuesta de eliminar persona 
![Eliminar persona](imgs/delete-response.png)

## Licencia 🔥
Copyright © 2020-present [Oscar Amado](https://github.com/ofaaoficial) 🧔
