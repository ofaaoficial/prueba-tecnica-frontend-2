import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Person} from "../models/Person";

@Injectable({
  providedIn: 'root'
})
export class PersonsService {

  private REST_API_SERVER = 'http://localhost:3200/api';

  constructor(private httpClient: HttpClient) {
  }

  all() {
    return this.httpClient.get(`${this.REST_API_SERVER}/people`);
  }

  create(person: Person) {
    return this.httpClient.post(`${this.REST_API_SERVER}/people`, person);
  }

  find(id: number) {
    return this.httpClient.get(`${this.REST_API_SERVER}/people/${id}`);
  }

  delete(id: number) {
    return this.httpClient.delete(`${this.REST_API_SERVER}/people/${id}`);
  }

  update(id: number, person: Person) {
    return this.httpClient.put(`${this.REST_API_SERVER}/people/${id}`, person);
  }
}
