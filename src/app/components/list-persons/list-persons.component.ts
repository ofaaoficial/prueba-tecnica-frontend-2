import {Component, OnInit} from '@angular/core';
import {PersonsService} from "../../services/persons.service";
import {Person} from "../../models/Person";
import swal from 'sweetalert';
import {Router} from "@angular/router";

@Component({
  selector: 'app-list-persons',
  templateUrl: './list-persons.component.html',
  styleUrls: ['./list-persons.component.css']
})
export class ListPersonsComponent implements OnInit {

  people: Person[];

  constructor(
    private personsService: PersonsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.personsService.all()
      .subscribe((response: Person[]) => this.people = response, console.log);
  }

  edit(id: number) {
    return this.router.navigate([`modificar/${id}`])
  }

  delete(id: number) {
    swal({
      title: '¿Estas seguro?',
      text: 'No podrá recuperar este registro!',
      icon: 'warning',
      buttons: ['Cancelar', 'Eliminar'],
      dangerMode: true
    }).then(value => {
      if (value) {
        this.personsService.delete(id).subscribe((response) => {
          this.people = this.people.filter((person) => person.id !== id);
          swal({
            title: "Bien!",
            text: "Registro eliminado correctamente!",
            icon: "success",
          });
        }, () => {
          swal({
            title: "Algo no esta bien!",
            text: "No puede eliminar un padre!",
            icon: "error",
          });
        })
      }
    });
  }
}
