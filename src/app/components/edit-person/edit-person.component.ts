import {Component, OnInit} from '@angular/core';
import {Person} from "../../models/Person";
import {PersonsService} from "../../services/persons.service";
import {ActivatedRoute, Router} from "@angular/router";
import swal from "sweetalert";

@Component({
  selector: 'app-edit-person',
  templateUrl: './edit-person.component.html',
  styleUrls: ['./edit-person.component.css']
})
export class EditPersonComponent implements OnInit {
  person: Person;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private personsService: PersonsService
  ) { }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.params.id;
    if (id) {
      this.personsService.find(id)
        .subscribe((response: Person) => this.person = response);
    } else {
      swal({
        title: "Algo no esta bien!",
        text: "El parámetro id es obligatorio!",
        icon: "error"
      }).then(() => {
        this.router.navigate(['/']);
      });
    }
  }

}
