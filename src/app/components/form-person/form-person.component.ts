import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Person} from "../../models/Person";
import {PersonsService} from "../../services/persons.service";
import swal from 'sweetalert';
import {Router} from "@angular/router";

@Component({
  selector: 'app-form-person',
  templateUrl: './form-person.component.html',
  styleUrls: ['./form-person.component.css']
})
export class FormPersonComponent implements OnInit {
  @Input() personData: Person;
  person: Person;
  form: FormGroup;
  people: Person[];

  constructor(
    private formBuilder: FormBuilder,
    private personsService: PersonsService,
    private router: Router) {
  }

  emptyPerson() {
    return {
      id: null,
      birth: '',
      fullname: '',
      mother_id: null,
      father_id: null
    };
  }

  ngOnInit() {
    this.personsService.all().subscribe((response: Person[]) => this.people = response);
    console.log(this.personData);
    this.person = this.personData ? this.personData : this.emptyPerson();

    this.form = this.formBuilder.group({
      fullname: [this.person.fullname, Validators.required],
      birth: [this.person.birth, Validators.required],
      father_id: [this.person.father_id],
      mother_id: [this.person.mother_id],
    })
  }

  get fullname() {
    return this.form.get('fullname');
  }

  get birth() {
    return this.form.get('birth');
  }

  get father_id() {
    return this.form.get('father_id');
  }

  get mother_id() {
    return this.form.get('mother_id');
  }

  save() {
    if (!this.personData) {
      this.personsService.create(this.form.getRawValue())
        .subscribe(() => {
          swal({
            title: "Bien!",
            text: "Persona registrada correctamente!",
            icon: "success"
          }).then(() => {
            this.router.navigate(['/']);
          });
        }, console.log);
    } else {
      this.personsService.update(this.person.id, this.form.getRawValue())
        .subscribe(() => {
          swal({
            title: "Bien!",
            text: "Persona actualizada correctamente!",
            icon: "success"
          }).then(() => {
            this.router.navigate(['/']);
          });
        }, console.log);
    }
  }

}
