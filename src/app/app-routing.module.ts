import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreatePersonComponent} from "./components/create-person/create-person.component";
import {ListPersonsComponent} from "./components/list-persons/list-persons.component";
import {EditPersonComponent} from "./components/edit-person/edit-person.component";


const routes: Routes = [
  {
    path: '',
    component: ListPersonsComponent
  },
  {
    path: 'crear',
    component: CreatePersonComponent
  },
  {
    path: 'modificar/:id',
    component: EditPersonComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
