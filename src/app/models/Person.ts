export interface Person {
  id?: number;
  fullname: string;
  birth: string;
  father_id: number;
  mother_id: number;
}
