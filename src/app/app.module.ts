import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ListPersonsComponent} from './components/list-persons/list-persons.component';
import {HttpClientModule} from "@angular/common/http";
import {PersonsService} from "./services/persons.service";
import {CreatePersonComponent} from './components/create-person/create-person.component';
import {FormPersonComponent} from './components/form-person/form-person.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { EditPersonComponent } from './components/edit-person/edit-person.component';

@NgModule({
  declarations: [
    AppComponent,
    ListPersonsComponent,
    CreatePersonComponent,
    FormPersonComponent,
    EditPersonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [PersonsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
